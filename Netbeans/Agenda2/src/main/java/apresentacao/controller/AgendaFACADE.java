package apresentacao.controller;

import apresentacao.model.Agenda;
import apresentacao.model.Compromisso;
import apresentacao.model.ListaAgenda;
import apresentacao.view.GerenciadorAgendas;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Rodrigo Frohlich - PCQS/HP
 */

public class AgendaFACADE {
    
    private ListaAgenda lista;
    private Agenda agenda;
    private Compromisso compromisso;
    
    public AgendaFACADE(){
        this.lista = new ListaAgenda();
    }
    
    public void criaAgenda(String titulo){
        agenda = new Agenda(titulo);
        
        if(titulo.length() > 0){
            this.lista.adicionar(agenda);
            JOptionPane.showMessageDialog(null, "Agenda criada com sucesso!");
        }
    }

    public ListaAgenda getLista() {
        return lista;
    }
    
    public void setLista(ListaAgenda lista){
        this.lista = lista;
    }
    
    public ArrayList<Compromisso> getListaComp(){
        return agenda.listarComp();
    }
    
    public void removeAllComp(){
        agenda.excluirAll();
    }
    
    public void removeComp(Compromisso c){
        agenda.excluir(c);
    }
    
    public void criaComp(String titulo, 
                        String assunto, 
                        String local, 
                        Date dataInicial, 
                        String hrInicial, 
                        String hrFinal){
        
        this.compromisso = new Compromisso();
        
        if(lista.listar().isEmpty()){
            
            JOptionPane.showMessageDialog(null, "Não existe nenhuma agenda criada!");
            
        }else if(titulo != "" && titulo != null && 
                assunto != "" && assunto != null &&
                local != "" && local != null &&
                dataInicial != null &&
                hrInicial != null &&
                hrFinal != null){
            
            compromisso.setTituloComp(titulo);
            compromisso.setAssuntoComp(assunto);
            compromisso.setLocalComp(local);
            compromisso.setDataInicialComp(dataInicial);
            compromisso.setHrInicialComp(hrInicial);
            compromisso.setHrFinalComp(hrFinal);
            
            agenda.adicionar(compromisso);
            
            JOptionPane.showMessageDialog(null, "Compromisso inserido com sucesso!");
            
        }else{
            JOptionPane.showMessageDialog(null, "Campos obrigatorios (*)");
        }  
    }
    
    public void removeAgenda(Agenda agenda){
        this.lista.excluir(agenda);
    }
    
    public ArrayList<Compromisso> compDia(Date data){
        ArrayList<Compromisso> listaDia = new ArrayList<>();
        for(Compromisso c : agenda.listarComp()){
            if(c.getDataInicial().getDate() == data.getDate()){
                listaDia.add(c);
            }
        }
        
        return listaDia;
    }
    
    public ArrayList<Compromisso> compSemana(Date data){
        ArrayList<Compromisso> listaSem = new ArrayList<>();
        for(Compromisso c : agenda.listarComp()){
            if(c.getDataInicial().getDate() >= data.getDate() && c.getDataInicial().getDate() <= (data.getDate()+ (6 - data.getDay()))){
                listaSem.add(c);
            }
        }
        
        return listaSem;
    }

    public ArrayList<Compromisso> compMes(Date data){
        ArrayList<Compromisso> listaMes = new ArrayList<>();
        for(Compromisso c : agenda.listarComp()){
            if(c.getDataInicial().getDate() >= data.getDate() && c.getDataInicial().getMonth() == data.getMonth()){
                listaMes.add(c);
            }
        }
        
        return listaMes;
    }
    
    public void setListaComp(ArrayList<Compromisso> lista){
        this.agenda.setListaComp(lista);
    }
}
