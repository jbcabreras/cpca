/*
 * O usuário cria diversas agendas. Uma agenda deve possuir um título.
 * O usuário cadastra compromissos em qualquer data válida do calendário. Um
 * compromisso pode ser acrescentado a qualquer agenda. Um compromisso deve possuir
 * um título, um assunto, um local, hora de início e hora de término.
 * O usuário visualiza os compromissos de cada agenda através de diferentes filtros, tais
 * como dia atual, semana, mês.
 * O usuário remove ou edita os dados de qualquer compromisso/agenda.
 * O usuário exporta os dados de um compromisso para um formato compatível com
 * outros sistemas. O formato escolhido é o iCalendar 
 */

package apresentacao.model;

import java.util.ArrayList;
import java.util.Comparator;
import static java.util.Comparator.comparing;
import java.util.Iterator;

/**
 *
 * @author Rodrigo Frohlich - PCQS/HP
 */

public class Agenda  {
    private String tituloAgenda;
    private ArrayList<Compromisso> listaComp;
    
    public Agenda(){
        this.listaComp = new ArrayList<>();
    }
    
    public Agenda(String titulo){
        this.listaComp = new ArrayList<>();
        this.tituloAgenda = titulo;
    }
    
    public void adicionar(Compromisso comp){
        this.listaComp.add(comp);
    }
    
    public void excluir(Compromisso comp){
        if(listaComp.contains(comp)){
            listaComp.remove(comp);
        }
    }
    
    public void excluirAll(){
        if(!listaComp.isEmpty()){
            listaComp.removeAll(listaComp);
        }
    }

    public String getTituloAgenda() {
        return tituloAgenda;
    }

    public void setTituloAgenda(String tituloAgenda) {
        this.tituloAgenda = tituloAgenda;
    }
    
    public ArrayList<Compromisso> listarComp(){
        listaComp.sort(comparing((Compromisso c) -> c.getDataInicialCompString()).thenComparing((Compromisso c) -> c.getHrInicialComp()));
        return this.listaComp;
    }
    
    @Override
    public String toString(){
       return this.getTituloAgenda();
    }
    
    public void setListaComp(ArrayList<Compromisso> lista){
        this.listaComp = lista;
    }

    public ArrayList<Compromisso> getListaComp() {
        return listaComp;
    }
    
    
}
