import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class JanelaPrincipal extends JFrame implements ActionListener
{
    private JList lista;
    private Vector lista_dados;
    private JButton botao;
    
    public JanelaPrincipal()
    {
        super();
        lista_dados = new Vector();
        inicializaDados();
        inicializaLista();
        botao = new JButton("Selecionar");
        botao.addActionListener(this);
        JPanel painel = new JPanel();
        painel.add(new JScrollPane(lista));
        painel.add(botao);
        this.getContentPane().add(painel);
        this.pack();
    }

    private void inicializaDados()
    {
        lista_dados.add("D�lar");
        lista_dados.add("D�lar Mexicano");
        lista_dados.add("Euro");
        lista_dados.add("Nuevo Sol");
        lista_dados.add("Pataca");
        lista_dados.add("Peso");
        lista_dados.add("Pila");
        lista_dados.add("Real");
    }
    
    private void inicializaLista()
    {
        lista = new JList(lista_dados);
        lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    public void actionPerformed(ActionEvent e)
    {
        JOptionPane.showMessageDialog(null, "Indice = " + lista.getSelectedIndex()
            + "\nValor = " + lista.getSelectedValue());
    }
}
