import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class JanelaPrincipal2 extends JFrame
{
    private JList lista;
    private DefaultListModel lista_dados;
    private JButton botao, botao2;
    
    public JanelaPrincipal2()
    {
        super();
        lista_dados = new DefaultListModel();
        inicializaDados();
        inicializaLista();
        botao = new JButton("Selecionar");
        botao.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                JOptionPane.showMessageDialog(null, "Indice = " + lista.getSelectedIndex()
                    + "\nValor = " + lista.getSelectedValue());
            }
        });
        botao2 = new JButton("Adicionar");
        botao2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                lista_dados.addElement("Novo!!!");
            }
        }); 
        JPanel painel = new JPanel();
        painel.add(new JScrollPane(lista));
        painel.add(botao);
        painel.add(botao2);
        this.getContentPane().add(painel);
        this.pack();
    }

    private void inicializaDados()
    {
        lista_dados.addElement("D�lar");
        lista_dados.addElement("D�lar Mexicano");
        lista_dados.addElement("Euro");
        lista_dados.addElement("Nuevo Sol");
        lista_dados.addElement("Pataca");
        lista_dados.addElement("Peso");
        lista_dados.addElement("Pila");
        lista_dados.addElement("Real");
    }
    
    private void inicializaLista()
    {
        lista = new JList(lista_dados);
        lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
}
