import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Janela extends JFrame 
{

  private Estados estados;

  private JScrollPane jScrollPane1 = new JScrollPane();
  private JList listaEstados = new JList();
  private JButton botaoSelecionar = new JButton();
  private JButton botaoAdicionar = new JButton();

  public Janela(Estados e)
  {
    this();
    estados = e;
    listaEstados.setModel(estados);
  }
  public Janela()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

  }

  private void jbInit() throws Exception
  {
    this.getContentPane().setLayout(null);
    this.setSize(new Dimension(400, 300));
    this.setTitle("JList Avan�ado");
    jScrollPane1.setBounds(new Rectangle(60, 30, 275, 195));
    listaEstados.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    botaoSelecionar.setText("Selecionar");
    botaoSelecionar.setBounds(new Rectangle(90, 235, 90, 20));
    botaoSelecionar.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          botaoSelecionar_actionPerformed(e);
        }
      });
    botaoAdicionar.setText("Adicionar");
    botaoAdicionar.setBounds(new Rectangle(205, 235, 90, 20));
    botaoAdicionar.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          botaoAdicionar_actionPerformed(e);
        }
      });
    jScrollPane1.getViewport().add(listaEstados, null);
    this.getContentPane().add(botaoAdicionar, null);
    this.getContentPane().add(botaoSelecionar, null);
    this.getContentPane().add(jScrollPane1, null);
  }

  private void botaoSelecionar_actionPerformed(ActionEvent e)
  {
    JOptionPane.showMessageDialog(this, "Selecionado: " + listaEstados.getSelectedValue());
  }

  private void botaoAdicionar_actionPerformed(ActionEvent e)
  {
    String sigla = JOptionPane.showInputDialog("Sigla:");
    String nome = JOptionPane.showInputDialog("Nome:");
    estados.adicionar(new Estado(sigla, nome));
  }
}