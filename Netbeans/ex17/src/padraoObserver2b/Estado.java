public class Estado
{
  private String sigla;
  private String nome;
  
  public Estado(String s, String n)
  {
    sigla = s;
    nome = n;
  }
  
  public String obterSigla()
  {
    return sigla;
  }
  
  public String obterNome()
  {
    return nome;
  }
  
  public String toString()
  {
    return this.obterSigla();
  }
}