package apresentacao.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

/**
 *
 * @author Rodrigo Frohlich - PCQS/HP
 */

@XStreamAlias("compromisso")
public class Compromisso {
    private String tituloComp;
    private String assuntoComp;
    private String localComp;
    private Date   dataInicialComp;
    private Date   dataFinalComp;
    private String hrInicialComp;
    private String hrFinalComp;
    
    public Compromisso(){}
    
    public Compromisso(String titulo, 
                        String assunto, 
                        String local, 
                        Date dataInicial, 
                        Date dataFinal,
                        String hrInicial, 
                        String hrFinal){
        
        this.tituloComp = titulo;
        this.assuntoComp = assunto;
        this.localComp = local;
        this.dataInicialComp = dataInicial;
        this.dataFinalComp = dataFinal;
        this.hrInicialComp = hrInicial;
        this.hrFinalComp = hrFinal;
    }

    public String getTituloComp() {
        return tituloComp;
    }

    public void setTituloComp(String tituloComp) {
        this.tituloComp = tituloComp;
    }

    public String getAssuntoComp() {
        return assuntoComp;
    }

    public void setAssuntoComp(String assuntoComp) {
        this.assuntoComp = assuntoComp;
    }

    public String getLocalComp() {
        return localComp;
    }

    public void setLocalComp(String localComp) {
        this.localComp = localComp;
    }
    
    public Date getDataInicial(){
        return dataInicialComp;
    }

    public String getDataInicialCompString() {
        String dataz = "dd/MM/yyyy";
        SimpleDateFormat formatas = new SimpleDateFormat(dataz );
        String data = formatas.format(dataInicialComp );
        
        return data;
    }

    public void setDataInicialComp(Date dataInicialComp) {
        this.dataInicialComp = dataInicialComp;
    }
    
    public Date getDataFinal(){
        return dataFinalComp;
    }

    public String getDataFinalCompString() {
        String dataz = "dd/MM/yyyy";
        SimpleDateFormat formatas = new SimpleDateFormat(dataz );
        String data = formatas.format(dataFinalComp );
        
        return data;
    }

    public void setDataFinalComp(Date dataFinalComp) {
        this.dataFinalComp = dataFinalComp;
    }

    public String getHrInicialComp() {
        return hrInicialComp;
    }

    public void setHrInicialComp(String hrInicialComp) {
        this.hrInicialComp = hrInicialComp;
    }

    public String getHrFinalComp() {
        return hrFinalComp;
    }

    public void setHrFinalComp(String hrFinalComp) {
        this.hrFinalComp = hrFinalComp;
    }

    @Override
    public String toString() {
        return this.getDataInicialCompString() + " (" + this.getHrInicialComp() + "hrs) - Título: " + this.getTituloComp();
    }
}

