package apresentacao.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.ArrayList;

/**
 *
 * @author Rodrigo Frohlich - PCQS/HP
 */

@XStreamAlias("lista")
public class ListaAgenda {
    private ArrayList<Agenda> listaAgenda;

    public ListaAgenda() {
        this.listaAgenda = new ArrayList<>();
    }
    
    public void adicionar(Agenda agenda){
        this.listaAgenda.add(agenda);
    }
    
    public void excluir(Agenda agenda){
        if(listaAgenda.contains(agenda)){
            listaAgenda.remove(agenda);
        }
    }
    
    public ArrayList<Agenda> listar(){
        return listaAgenda;
    }
    
}
