import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

public class Estados extends AbstractListModel 
{
  private List<Estado> elementos;
  
  public Estados()
  {
    elementos = new ArrayList<Estado>();
  }
  
  public void adicionar(Estado e)
  {
    elementos.add(e);
    //disparar evento avisando que a cole��o de dados foi alterada
    this.fireIntervalAdded(this,elementos.size()-1,elementos.size()-1);
  }
  
  //m�todo da interface ListModel que retorna o n�mero de elementos da lista
  public int getSize()
  {
    return elementos.size();
  }
  
  //m�todo da interface ListModel que retorna o elemento da posi��o indicada
  public Object getElementAt(int i)
  {
    return elementos.get(i);
  }
}