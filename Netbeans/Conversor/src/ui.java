
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pcqs_2
 */
public class ui extends javax.swing.JFrame {

    /**
     * Creates new form ui
     */
    public ui() {
        initComponents();
        icone();
       
    }
    
    public void icone(){          
        Image iconeTitulo = Toolkit.getDefaultToolkit().getImage("./img/icone.png");           
        setIconImage(iconeTitulo);        
    }  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgMenu = new javax.swing.ButtonGroup();
        radioCF = new javax.swing.JRadioButton();
        radioFC = new javax.swing.JRadioButton();
        campoEntrada = new javax.swing.JTextField();
        botaoConverter = new javax.swing.JToggleButton();
        campoSaida = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Conversor de temperaturas 1.0");
        setName(""); // NOI18N

        bgMenu.add(radioCF);
        radioCF.setSelected(true);
        radioCF.setText("º C   >>   º F");
        radioCF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCFActionPerformed(evt);
            }
        });

        bgMenu.add(radioFC);
        radioFC.setText("º F   >>   º C");
        radioFC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioFCActionPerformed(evt);
            }
        });

        campoEntrada.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                campoEntradaFocusGained(evt);
            }
        });
        campoEntrada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                campoEntradaKeyTyped(evt);
            }
        });

        botaoConverter.setText(">>");
        botaoConverter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                botaoConverterMouseClicked(evt);
            }
        });
        botaoConverter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoConverterActionPerformed(evt);
            }
        });

        campoSaida.setEditable(false);
        campoSaida.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        campoSaida.setForeground(new java.awt.Color(255, 51, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 51, 255));
        jLabel1.setText("Sobre..");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioCF)
                            .addComponent(radioFC))
                        .addGap(18, 18, 18)
                        .addComponent(campoEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoConverter, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(campoSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(radioCF)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radioFC))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(campoSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(campoEntrada)
                                .addComponent(botaoConverter, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void botaoConverterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoConverterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botaoConverterActionPerformed

    private void radioCFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioCFActionPerformed

    private void botaoConverterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botaoConverterMouseClicked
        // TODO add your handling code here:
        if(!campoEntrada.getText().equals("")){
            if(radioCF.isSelected()){
                double c = 0, f = 0;
                String resultado = null;

                if(campoEntrada.getText().matches("[\\d]*.?[\\d]*")){
                    c = Double.parseDouble(campoEntrada.getText());

                    f = ((c / 5) * 9) + 32;

                    f = (double) Math.round(f * 100) / 100;

                    resultado = String.valueOf(f);
                    campoSaida.setText(resultado + " ºF");
                }else{
                    JOptionPane.showMessageDialog(null, "Somente números!");
                } 
            }else if(radioFC.isSelected()){
                double f = 0, c = 0;
                String resultado = null;

                if(campoEntrada.getText().matches("[\\d]*.?[\\d]*")){
                    f = Double.parseDouble(campoEntrada.getText());

                    c = ((f - 32) / 9) * 5;

                    c = (double) Math.round(c * 100) / 100;

                    resultado = String.valueOf(c);
                    campoSaida.setText(resultado + " ºC");
                }else{
                    JOptionPane.showMessageDialog(null, "Somente números!");
                } 
            } 
        }else{
            JOptionPane.showMessageDialog(null, "Favor informar um valor!");
        }
        
    }//GEN-LAST:event_botaoConverterMouseClicked

    private void campoEntradaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_campoEntradaKeyTyped
        // TODO add your handling code here:
        
        char tecla = evt.getKeyChar();
        String cod = campoEntrada.getText();
        int ponto = cod.indexOf(".") + 1;
                
        if(ponto == 0){
            if(!Character.isDigit(tecla) && tecla != KeyEvent.VK_BACK_SPACE && tecla != KeyEvent.VK_PERIOD || campoEntrada.getText().length() >= 8){
                evt.consume();
                getToolkit().beep();
            }
        }else{
            if(!Character.isDigit(tecla) && tecla != KeyEvent.VK_BACK_SPACE || campoEntrada.getText().length() >= 8){
                evt.consume();
                getToolkit().beep();
            }
        }
    }//GEN-LAST:event_campoEntradaKeyTyped

    private void campoEntradaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_campoEntradaFocusGained
        // TODO add your handling code here:
         
    }//GEN-LAST:event_campoEntradaFocusGained

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel1MouseEntered

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Somente números inteiros ou reais (ex.: 32.79)");
    }//GEN-LAST:event_jLabel1MouseClicked

    private void radioFCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioFCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioFCActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ui().setVisible(true);
            }            
        });
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgMenu;
    private javax.swing.JToggleButton botaoConverter;
    private javax.swing.JTextField campoEntrada;
    private javax.swing.JTextField campoSaida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JRadioButton radioCF;
    private javax.swing.JRadioButton radioFC;
    // End of variables declaration//GEN-END:variables
}
