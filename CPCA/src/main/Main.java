package main;

import exercicios.Cofrinho;
import exercicios.Moeda;

public class Main {

	public static void main(String[] args) {
//		Circulo c = new Circulo(2, 3, 4);
//		System.out.println(c.toString());
//		
//		c.moverCirculo(1, 2);
//		System.out.println(c.toString());
//		
//		c.aumentaTamanho();
//		System.out.println(c.toString());
//
//		c.diminuiTamanho();
//		System.out.println(c.toString());
		
//		Ponto a = new Ponto(1,2);
//		Ponto b = new Ponto(2,3);
//		
//		System.out.println(a.calculaDistancia(b));
//		System.out.println(a.calculaDistancia(2,3));
//		System.out.println(a.calculaDistancia(1,2,2,3));
		
//		Lampada l = new Lampada(100, 220);
//		System.out.println(l.estado());
//		
//		l.ligar();
//		System.out.println(l.estado());
//		
//		l.desligar();
//		System.out.println(l.estado());
//		System.out.println("Potencia: "+l.getPotencia()+", voltagem: "+l.getVoltagem());
		
//		Ponto p = null;
//		Circulo c = new Circulo(p, 2);
		
//		Ponto p2 = new Ponto(1,2);
//		Circulo c2 = new Circulo(p2, 2);
//		System.out.println(c2.toString());
		
//		Moeda m1 = new Moeda(0.01, "real");
//		Moeda m2 = new Moeda(0.02, "real");
//		Moeda m7 = new Moeda(0.05, "real");
//		Moeda m3 = new Moeda(0.10, "real");
//		Moeda m4 = new Moeda(0.25, "real");
//		Moeda m5 = new Moeda(0.50, "real");
//		Moeda m6 = new Moeda(1, "real");
//		Moeda m8 = new Moeda(0.25, "real");
//		Moeda m9 = new Moeda(0.50, "real");
//		Moeda m10 = new Moeda(1, "real");
//		
//		Cofrinho c1 = new Cofrinho();
//		
//		c1.adicionar(m6);
//		c1.adicionar(m5);
//		c1.adicionar(m4);
//		c1.adicionar(m3);
//		c1.adicionar(m1);
//		c1.adicionar(m2);
//		c1.adicionar(m7);
//		c1.adicionar(m8);
//		c1.adicionar(m9);
//		c1.adicionar(m10);
//		
//		for(Moeda m : c1){
//			System.out.println(m.getValor());
//		}
//		
//		c1.forEach(m -> System.out.println(m.getValor()));
//		
//		System.out.println(c1.calcularTotal());
//		System.out.println(c1.valorMenorMoeda());
//		c1.listaMoedas();
//		for(Map.Entry<Double, Integer> chaveValor : c1.listaMoedas().entrySet()){
//			System.out.println(chaveValor.getKey()+" = "+chaveValor.getValue());
//		}
		
//		MatrizQuadrada m = new MatrizQuadrada(5);
//		
//		for(int i = 0; i < 5; i++){
//			for(int j = 0; j < 5; j++){
//				m.setValor(i, j, i + 1);
//			}
//		}
//
//		MatrizQuadrada.imprimeMatriz(m);
//		System.out.println();
//
//		MatrizQuadrada m3 = new MatrizQuadrada(4);
//		
//		for(int i = 0; i < 4; i++){
//			for(int j = 0; j < 4; j++){
//				m3.setValor(i, j, i + 6);
//			}
//		}
//		
//		MatrizQuadrada.imprimeMatriz(m3);
//		System.out.println();
//		
//		MatrizQuadrada m2 = new MatrizQuadrada(5);
//		
//		for(int i = 0; i < 5; i++){
//			for(int j = 0; j < 5; j++){
//				m2.setValor(i, j, i + 2);
//			}
//		}
//		
//		MatrizQuadrada.imprimeMatriz(m2);
//		
//		
//		System.out.println();
//		
//		MatrizQuadrada.imprimeMatriz(m.somaMatriz(m2));
//		
//		System.out.println();
//		
//		MatrizQuadrada.imprimeMatriz(m.somaMatriz(m3));
		
//		for(int i = 0; i < 5; i++){
//			for(int j = 0; j < 5; j++){
//				System.out.println(m.getValor(i, j));
//			}
//		}
		
//		System.out.println(m.somaLinha(3));
//		System.out.println(m.somaColuna(4));
//		System.out.println(m.somaDiagonal());
		
//		Pessoa p1 = new Pessoa("joao", "1");
//		Pessoa p2 = new Pessoa("maria", "2");
//		
//		ContaCorrente cc = new ContaCorrente(1, p1);
//		ContaCorrente cp = new ContaPoupanca(2, p2);
//		
//		CadastroContas cad = new CadastroContas();
//		cad.adicionar(cc);
//		cad.adicionar(cp);
//		
//		for(ContaCorrente conta : (ArrayList<ContaCorrente>) cad.buscar()){
//			System.out.println(conta.getNumero());
//		}
		
	}
}
