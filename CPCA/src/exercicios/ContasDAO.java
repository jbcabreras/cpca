package exercicios;

import java.util.List;

public interface ContasDAO {
	public void adicionar(ContaCorrente conta);
	
	public List buscar();
	
	public ContaCorrente buscar(int nrConta);
	
	public List buscar(String cfpTitular);
}
