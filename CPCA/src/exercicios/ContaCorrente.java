package exercicios;

import java.util.List;

public class ContaCorrente {
	private int numero;
	private double saldo;
	private Pessoa titular;
	
	public ContaCorrente(int umNumero, Pessoa umTitular){
		this.numero = umNumero;
		this.titular = umTitular;
	}

	public int getNumero() {
		return numero;
	}

	public double getSaldo() {
		return saldo;
	}

	public Pessoa getTitular() {
		return titular;
	}
	
	public void depositar(double valor){
		this.saldo += valor;
	}
	
	public void sacar(double valor){
		this.saldo -= valor;
	}

}
