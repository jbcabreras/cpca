package exercicios;

public class Pessoa {
	private String nome;
	private String cpf;
	
	public Pessoa(String umNome, String umCpf){
		this.nome = umNome;
		this.cpf = umCpf;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}
}
