//3) Crie uma classe Ponto, com a capacidade de armazenar as coordenadas cartesianas X e Y
//atrav�s de valores inteiros. Implemente tr�s construtores: um construtor sem par�metros, que
//cria um ponto nas coordenadas (0,0), um construtor que recebe dois par�metros de
//coordenadas X e Y, e um construtor que inicializa o ponto atrav�s das coordenadas de um
//outro ponto recebido como argumento.

//4) Utilizando a classe Ponto, introduza novos m�todos na classe Circulo, atrav�s da sobrecarga,
//para realizar as seguintes fun��es:
// construir novos c�rculos atrav�s da informa��o de um ponto central e raio;
// construir novos c�rculos atrav�s da informa��o de um ponto central e di�metro;
// mover um c�rculo para um novo ponto central qualquer.


//5) Acrescente � classe Ponto, desenvolvida anteriormente, a capacidade de calcular a dist�ncia
//entre dois pontos. Para tal, � desejado o seguinte comportamento:
// calcular a dist�ncia entre a inst�ncia do ponto e um outro objeto ponto qualquer;
// calcular a dist�ncia entre a inst�ncia do ponto e um outro ponto dado pelas
//coordenadas X e Y;
// calcular a dist�ncia entre dois pontos dadas as coordenadas X1, Y1 e X2, Y2.
//A dist�ncia entre dois pontos � calculada por:


package exercicios;

public class Ponto {
	private int x;
	private int y;
	
	public Ponto(){
		this.x = 0;
		this.y = 0;
	}

	public Ponto(int x, int y){
		this.x = x;
		this.y = y;
	}

	public Ponto(Ponto p){
		this.x = p.getX();
		this.y = p.getY();
	}

	public int getX() {
		return x;
	}
	
	public double calculaDistancia(Ponto p1, Ponto p2){
		double x1, y1, x2, y2, distancia;
		
		x1 = p1.getX();
		x2 = p2.getX();
		y1 = p1.getY();
		y2 = p2.getY();
		
		distancia = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		
		return distancia;
	}

	public double calculaDistancia(Ponto p2){
		double x1, y1, x2, y2, distancia;
		
		x1 = this.x;
		x2 = p2.getX();
		y1 = this.y;
		y2 = p2.getY();
		
		distancia = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		
		return distancia;
	}

	public double calculaDistancia(double x2, double y2){
		double x1, y1, distancia;
		
		x1 = this.x;
		y1 = this.y;
		
		distancia = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		
		return distancia;
	}

	public double calculaDistancia(double x1, double y1, double x2, double y2){
		double distancia;
		
		distancia = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		
		return distancia;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	
}
