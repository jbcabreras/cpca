//9) Alterar a classe Cofrinho do exerc�cio anterior para incluir:
// M�todo que retorna o valor da menor moeda armazenada.
// M�todo que retorna uma inst�ncia da menor moeda armazenada.
// M�todo que retorna um mapa/dicion�rio com a frequ�ncia (o n�mero de moedas) de
//cada moeda existente (1 centavo, 5 centavos, 10 centavos, 25 centavos, 50 centavos, 1
//real) no cofrinho.
//Na implementa��o dos m�todos, caso o Cofrinho esteja vazio, qual seria uma boa solu��o de
//implementa��o em cada caso?

package exercicios;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Cofrinho implements Iterable<Moeda>{
	private ArrayList<Moeda> lista = new ArrayList<>();
	
	public Cofrinho(){}
	
	public void adicionar(Moeda moeda){
		if(moeda != null){
			lista.add(moeda);
		}
	}
	
	public double calcularTotal(){
		if(lista.isEmpty()){
			throw new IllegalArgumentException("O cofrinho est� vazio.");
		}else{
			double total = 0;
			for(Moeda m : lista){
				total += m.getValor();
			}
			return total;
		}
	}
	
	public double valorMenorMoeda(){
		if(lista.isEmpty()){
			throw new IllegalArgumentException("O cofrinho est� vazio.");
		}else{
			
			return Collections.min (lista).getValor();
		}
	}
	
	public Moeda moedaMenorValor(){
		if(lista.isEmpty()){
			throw new IllegalArgumentException("O cofrinho est� vazio.");
		}else{
			
			return Collections.min (lista);
		}
	}
	
	public Map<Double, Integer> listaMoedas(){
		if(lista.isEmpty()){
			throw new IllegalArgumentException("O cofrinho est� vazio.");
		}else{
			Map<Double, Integer> contador = new HashMap<>();
			for(Moeda moeda : lista){
				if(contador.containsKey(moeda.getValor())){
					contador.replace(moeda.getValor(), contador.get(moeda.getValor())+1);
				}else{
					contador.put(moeda.getValor(), 1);
				}
			}
			return contador;
		}
	}

	@Override
	public Iterator<Moeda> iterator() {
		return new IteradorMoedas();
	}
	
	private class IteradorMoedas implements Iterator<Moeda>{
		int contador = 1;
		
		@Override
		public boolean hasNext() {
			return contador < lista.size();
		}

		@Override
		public Moeda next() {
			Moeda proxima = null;
			if(this.hasNext()){
				proxima = lista.get(contador);
				contador++;
			}
			return proxima;
		}
	}
}
