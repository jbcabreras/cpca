//1) Adicione os seguintes comportamentos � classe Circulo:
// mover um circulo para qualquer posi��o;
// aumentar o tamanho de um c�rculo em uma unidade;
// diminuir o tamanho de um c�rculo em uma unidade;
// informar uma representa��o textual do c�rculo, contendo seu ponto central e raio.

//2) Defina novos construtores para a classe Circulo com as seguintes caracter�sticas:
// receba como par�metros o ponto X e Y para o centro, para o raio utilizar o valorpadr�o 1;
// receba como par�metro o raio, para o ponto central X e Y utilizar o valor padr�o 0;
// receba como par�metro um outro c�rculo, inicializando os atributos com os mesmos valores do outro c�rculo.

package exercicios;

public class Circulo {
	private double x;
	private double y;
	private double raio;
	
	public Circulo(double x, double y, double raio){
		this.x = x;
		this.y = y;
		this.raio = raio;
	}

	public Circulo(double x, double y){
		this.x = x;
		this.y = y;
		this.raio = 1;
	}

	public Circulo(double raio){
		this.x = 0;
		this.y = 0;
		this.raio = raio;
	}

	public Circulo(Circulo c){
		this.x = c.getX();
		this.y = c.getY();
		this.raio = c.getRaio();
	}
	
//	7) Dadas as classes Circulo e Ponto, reescreva o construtor Circulo(Ponto p, int r) para que:
//		 Gere uma exce��o NullPointerException caso receba null no par�metro do ponto;
//		 Gere uma exce��o IllegalArgumentException caso receba um n�mero n�o positivo no
//		par�metro do raio
//		
//	public Circulo(Ponto p, double raio){
//		this.x = p.getX();
//		this.y = p.getY();
//		this.raio = raio;
//	}
	
	public Circulo(Ponto p, double raio) throws NullPointerException, IllegalArgumentException{
		if(p == null){
			throw new NullPointerException("O ponto nao pode ser nulo.");
			 
		}else if(raio <= 0){
			IllegalArgumentException excecao = new IllegalArgumentException("Valor do raio deve ser maior que zero.");
			throw excecao;
		}else{
			this.x = p.getX();
			this.y = p.getY();
			this.raio = raio;
		}
	}

	public Circulo(double diametro, Ponto p){
		this.x = p.getX();
		this.y = p.getY();
		this.raio = diametro / 2;
	}
	
	public void moverCirculo(double x, double y){
		this.x = x;
		this.y = y;
	}

	public void moverCirculo(Ponto p){
		this.x = p.getX();
		this.y = p.getY();
	}
	
	public void aumentaTamanho(){
		this.raio += 1;
	}

	public void diminuiTamanho(){
		this.raio -= 1;
	}
	
	public String toString(){
		return "Ponto central: ("+x+", "+y+") - Raio: "+raio;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getRaio() {
		return raio;
	}

	public void setRaio(double raio) {
		this.raio = raio;
	}
	
	
}
