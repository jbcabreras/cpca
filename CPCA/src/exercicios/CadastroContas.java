package exercicios;

import java.util.ArrayList;
import java.util.List;

public class CadastroContas implements ContasDAO{
	private ArrayList<ContaCorrente> cadastro = new ArrayList<>();
	
	@Override
	public void adicionar(ContaCorrente conta) {
		this.cadastro.add(conta);
	}

	@Override
	public List buscar() {
		return cadastro;
	}

	@Override
	public ContaCorrente buscar(int nrConta) {
		for(ContaCorrente conta : cadastro){
			if(nrConta == conta.getNumero()){
				return conta;
			}
		}
		return null;
	}

	@Override
	public List buscar(String cfpTitular) {
		List lista = null;
		for(ContaCorrente conta : cadastro){
			if(conta.getTitular().getCpf().equals(cfpTitular)){
				lista.add(conta);
			}
		}
		return lista;
	}
}
