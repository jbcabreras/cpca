package exercicios;

public class Moeda implements Comparable<Moeda>{
	private double valor;
	private String nome;
	
	public Moeda(double valor, String nome){
		this.valor = valor;
		this.nome = nome;
	}

	public double getValor() {
		return valor;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public int compareTo(Moeda m) {
		return  this.valor < m.getValor() ? -1 : (this.valor > m.getValor() ? 1 : 0);
	}
}
