//6) Desenvolva uma abstra��o de uma l�mpada, a qual pode ser ligada e desligada. Sabe-se que
//a l�mpada pode queimar ao ser ligada com uma chance de 30%, e que uma vez queimada ela
//n�o pode mais ser ligada ou desligada novamente (Dica: utilize o gerador de n�meros
//aleat�rios de Java, classe Random, para sortear a chance de uma l�mpada queimar quando for
//ligada). Deve ser poss�vel observar o estado da l�mpada (se desligada, ligada ou queimada). A
//l�mpada tamb�m deve possui as caracter�sticas de pot�ncia e voltagem. Garanta que seja
//poss�vel ler os valores de pot�ncia e voltagem de uma l�mpada. Lembre-se que n�o � poss�vel
//alterar o estado da l�mpada diretamente a n�o ser atrav�s de opera��es de ligar e desligar.


package exercicios;

import java.util.Random;

public class Lampada {
	private boolean ligada;
	private boolean queimada;
	private int potencia;
	private int voltagem;
	
	public Lampada(int potencia, int voltagem){
		this.potencia = potencia;
		this.voltagem = voltagem;
	}
	
	public void ligar(){
		if(ligada == false && queimada == false){
			Random aleatorio = new Random();
			int n = aleatorio.nextInt(10);
			
			if(n > 2){
				this.ligada = true;
			}else{
				this.queimada = true;
			}
		}
	}
	
	public void desligar(){
		if(ligada == true && queimada == false){
			this.ligada = false;
		}
	}
	
	public String estado(){
		if(queimada == true){
			return "Queimada";
		}else if(ligada == true){
			return "Ligada";
		}else{
			return "Desligada";
		}
	}
	
	public int getPotencia(){
		return this.potencia;
	}

	public int getVoltagem(){
		return this.voltagem;
	}
}
