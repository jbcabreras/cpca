package exercicios;

public class MatrizQuadrada {
	private int[][] matriz;
	private int tamanho;
	
	public MatrizQuadrada(int tamanho){
		if(tamanho > -1){
			matriz = new int[tamanho][tamanho];
			this.tamanho = tamanho;
		}
	}
	
	public int getValor(int x, int y){
		return matriz[x][y];
	}
	
	public void setValor(int x, int y, int novoValor){
		this.matriz[x][y] = novoValor;
	}
	
	public int somaLinha(int linha){
		int soma = 0;
		for(int i = 0; i < matriz.length; i++){
			soma += matriz[linha][i];
		}
		return soma;
	}
	
	public int somaColuna(int coluna){
		int soma = 0;
		for(int i = 0; i < matriz.length; i++){
			soma += matriz[i][coluna];
		}
		return soma;
	}
	
	public int somaDiagonal(){
		int soma = 0;
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz.length; j++){
				if(i == j){
					soma += matriz[i][j];
				}
			}
		}
		return soma;
	}
	
	public MatrizQuadrada somaMatriz(MatrizQuadrada m2){
		if(this.tamanho == m2.tamanho){
			MatrizQuadrada r1 = new MatrizQuadrada(this.tamanho);
			for(int i = 0; i < this.tamanho; i++){
				for(int j = 0; j < this.tamanho; j++){
					r1.setValor(i, j, this.getValor(i, j) + m2.getValor(i, j));
				}
			}
			return r1;
		}else{
			MatrizQuadrada r2 = new MatrizQuadrada(this.tamanho + m2.tamanho);
			for(int i = 0; i < r2.tamanho; i++){
				for(int j = 0; j < r2.tamanho; j++){
					r2.setValor(i, j, 0);
				}
			}
			for(int i = 0; i < this.tamanho; i++){
				for(int j = 0; j < this.tamanho; j++){
					r2.setValor(i, j, this.getValor(i, j));
				}
			}
			for(int i = r2.tamanho -1; i >= r2.tamanho - m2.tamanho; i--){
				for(int j = r2.tamanho -1; j >= r2.tamanho - m2.tamanho; j--){
					r2.setValor(i , j, m2.getValor(i - this.tamanho, j - this.tamanho));
				}
			}
			return r2;
		}
	}
	
	public static void imprimeMatriz(MatrizQuadrada m){
		for(int i = 0; i < m.tamanho; i++){
			for(int j = 0; j < m.tamanho; j++){
				System.out.print(m.getValor(i, j) + " ");
			}
			System.out.println();
		}
	}
}
