package exercicios;

public class ContaPoupanca extends ContaCorrente {
	private double juros;

	public ContaPoupanca(int umNumero, Pessoa umTitular) {
		super(umNumero, umTitular);
	}

	public double getJuros() {
		return juros;
	}

	public void setJuros(double juros) {
		this.juros = juros;
	}
	
	public void depositarRendimento(double rendimento){
		super.depositar(rendimento);
	}
}
